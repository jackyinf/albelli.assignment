﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Albelli.Assignment.Api.Services;
using Albelli.Assignment.Domain.Dtos.Requests;
using Albelli.Assignment.Domain.Dtos.Results;
using Albelli.Assignment.Domain.Models;
using Albelli.Assignment.Domain.Repositories;
using Moq;
using NUnit.Framework;

namespace Albelli.Assignment.Api.Tests.UnitTests
{
    [TestFixture]
    public class CustomerServiceTests
    {
        private Mock<ICustomerRepository> _customerRepositoryMock;
        private Mock<IOrderRepository> _orderRepositoryMock;
        private readonly Customer _john;
        private readonly Customer _bill;
        private readonly Customer _fred;

        public CustomerServiceTests()
        {
            _john = new Customer { Name = "John", Email = "john@test.com" };
            _bill = new Customer { Name = "Bill", Email = "bill@test.com" };
            _fred = new Customer
            {
                Name = "Fred",
                Email = "fred@test.com",
                Orders = new List<Order>
                {
                    new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 5.99m, Title = "Burder"},
                    new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 3.99m, Title = "Fries"},
                    new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 2m, Title = "Toy"}
                }
            };
        }

        [SetUp]
        public void SetUp()
        {
            _customerRepositoryMock = new Mock<ICustomerRepository>();
            _orderRepositoryMock = new Mock<IOrderRepository>();
        }

        [Test]
        public async Task should_get_customers()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.GetCustomers()).ReturnsAsync(new List<Customer> { _john , _bill, _fred});

            // Act
            var result = await UnitUnderTest().GetCustomers();

            // Assert
            Assert.AreEqual(result.Payload.Count, 3);
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _john.Name));
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _bill.Name));
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _fred.Name));
            Assert.IsEmpty(result.Payload.Single(x => x.Name == _fred.Name).Orders);
        }

        [Test]
        public async Task should_get_customers_with_orders()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.GetCustomers()).ReturnsAsync(new List<Customer> { _john, _bill, _fred });

            // Act
            var result = await UnitUnderTest().GetCustomersWithOrders();

            // Assert
            Assert.AreEqual(result.Payload.Count, 3);
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _john.Name));
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _bill.Name));
            Assert.IsNotNull(result.Payload.SingleOrDefault(x => x.Name == _fred.Name));
            Assert.IsNotEmpty(result.Payload.Single(x => x.Name == _fred.Name).Orders);
        }

        [Test]
        public async Task should_not_add_order_because_of_invalid_customer_id()
        {
            // Act
            var result = await UnitUnderTest().AddNewOrderForCustomer(-1, new NewOrderDto());

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("Customer's ID must be positive number", result.Message);
        }

        [Test]
        public async Task should_not_add_order_because_of_invalid_dto()
        {
            // Act
            var result = await UnitUnderTest().AddNewOrderForCustomer(1, null);

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("No order info supplied", result.Message);
        }

        [Test]
        public async Task should_not_add_order_because_of_missing_customer()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.GetCustomer(999)).ReturnsAsync(() => null);

            // Act
            var result = await UnitUnderTest().AddNewOrderForCustomer(999, new NewOrderDto());

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("Customer with such id was not found", result.Message);
        }

        [Test]
        public async Task should_not_add_order_because_of_failed_validation()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.GetCustomer(1)).ReturnsAsync(() => _john);

            // Act
            var result = await UnitUnderTest().AddNewOrderForCustomer(1, new NewOrderDto());

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("Title is required; Price cannot be negative or 0", result.Message);
        }

        [Test]
        public async Task should_add_order_successfully()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.GetCustomer(55)).ReturnsAsync(() => _bill);

            // Act
            var result = await UnitUnderTest().AddNewOrderForCustomer(55, new NewOrderDto {Title = "TV", Price = 999});

            // Assert
            Assert.IsTrue(result.IsSuccessful);
            _orderRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Order>()), Times.Once);
            _orderRepositoryMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Test]
        public async Task should_not_add_customer_because_of_invalid_dto()
        {
            // Act
            var result = await UnitUnderTest().AddNewCustomer(null);

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("No customer info supplied", result.Message);
        }

        [Test]
        public async Task should_not_add_customer_because_of_existing_customer()
        {
            // Arrange
            _customerRepositoryMock.Setup(x => x.FindByAsync(_john.Email)).ReturnsAsync(_john);

            // Act
            var result = await UnitUnderTest().AddNewCustomer(new NewCustomerDto {Email = _john.Email, Name = "Bob"});

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("Email is already taken", result.Message);
        }

        [Test]
        public async Task should_not_add_customer_because_of_failed_validation()
        {
            // Act
            var result = await UnitUnderTest().AddNewCustomer(new NewCustomerDto());

            // Assert
            Assert.IsFalse(result.IsSuccessful);
            Assert.AreEqual("Name cannot be null; Email cannot be null", result.Message);
        }

        [Test]
        public async Task should_add_customer_successfully()
        {
            // Act
            var result = await UnitUnderTest().AddNewCustomer(new NewCustomerDto { Email = "success@test.com", Name = "Successful Man"});

            // Assert
            Assert.IsTrue(result.IsSuccessful);
            _customerRepositoryMock.Verify(x => x.AddAsync(It.IsAny<Customer>()), Times.Once);
            _customerRepositoryMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [DebuggerStepThrough]
        private CustomerService UnitUnderTest() => new CustomerService(_customerRepositoryMock.Object, _orderRepositoryMock.Object);
    }
}