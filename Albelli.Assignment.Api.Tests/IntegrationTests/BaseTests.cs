﻿using Albelli.Assignment.Api.Tests.Utils;
using Microsoft.Extensions.Configuration;

namespace Albelli.Assignment.Api.Tests.IntegrationTests
{
    public abstract class BaseTests
    {
        protected DbHelper DbHelper;
        
        protected BaseTests()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            DbHelper = new DbHelper(connectionString);
        }

    }
}