﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Albelli.Assignment.Api.Tests.Utils;
using Albelli.Assignment.DAL;
using Albelli.Assignment.Domain.Dtos.Requests;
using Albelli.Assignment.Domain.Dtos.Results;
using Albelli.Assignment.Domain.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Albelli.Assignment.Api.Tests.IntegrationTests
{
    [TestFixture]
    [Parallelizable(ParallelScope.None)]
    public class CustomerTests : BaseTests
    {
        private const string RoutePrefix = "api/customers";
        private readonly IWebHostBuilder _builder;
        private TestServer _server;

        public CustomerTests()
        {
            _builder = new WebHostBuilder()
                .UseStartup<Startup>()
                .ConfigureServices(collection => collection.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(DbHelper.ConnectionString)));
            DbHelper.DoDatabaseOperation(context => context.Database.Migrate());
            _server = new TestServer(_builder);
        }

        [SetUp]
        public void SetUp() => PrepareData();

        [OneTimeTearDown]
        public void OneTimeTearDown() => _server.Dispose();

        [Test]
        public async Task should_get_customers()
        {
            using (var client = _server.CreateClient())
            {
                var response = await client.GetAsync(RoutePrefix);
                response.EnsureSuccessStatusCode();
                var result = await response.Content.DeserializeFromJson<ServiceResult<List<CustomerDto>>>();

                Assert.NotNull(result);

                var customers = result.Payload;
                Assert.NotNull(customers);
                Assert.AreEqual(3, customers.Count);
                Assert.IsEmpty(customers.SelectMany(x => x.Orders));
            }
        }

        [Test]
        public async Task should_get_customers_with_orders()
        {
            using (var client = _server.CreateClient())
            {
                var response = await client.GetAsync($"{RoutePrefix}/with-orders");
                response.EnsureSuccessStatusCode();
                var result = await response.Content.DeserializeFromJson<ServiceResult<List<CustomerDto>>>();

                Assert.NotNull(result);

                var customers = result.Payload;
                Assert.NotNull(customers);
                Assert.AreEqual(3, customers.Count);

                var fredCustomer = customers.FirstOrDefault(x => x.Name == "Fred");
                Assert.NotNull(fredCustomer);
                var orders = fredCustomer.Orders;
                Assert.IsNotEmpty(orders);
                Assert.AreEqual(3, orders.Count);
            }
        }

        [Test]
        public async Task should_add_new_order_for_an_existing_customer()
        {
            using (var client = _server.CreateClient())
            {
                var newTitle = Guid.NewGuid().ToString().Replace("-", "");
                Customer fred = null;
                DbHelper.DoDatabaseOperation(context =>
                    {
                        fred = context.Customers.FirstOrDefault(x => x.Name == "Fred");
                        Assert.NotNull(fred);
                    });
                var response = await client.PostJsonAsync($"{RoutePrefix}/{fred.Id}/order", new NewOrderDto { Title = newTitle, Price = 999 });
                response.EnsureSuccessStatusCode();
                var result = await response.Content.DeserializeFromJson<ServiceResult<int>>();
                Assert.NotNull(result);
                var orderId = result.Payload;
                DbHelper.DoDatabaseOperation(context =>
                {
                    var newOrder = context.Orders.FirstOrDefault(x => x.Id == orderId);
                    Assert.NotNull(newOrder);
                    Assert.AreEqual(newTitle, newOrder.Title);
                    Assert.AreEqual(999, newOrder.Price);
                });
            }
        }

        [Test]
        public async Task should_add_a_new_customer()
        {
            using (var client = _server.CreateClient())
            {
                var newEmail = $"{Guid.NewGuid().ToString().Replace("-", "")}@test.com";
                var newName = Guid.NewGuid().ToString().Replace("-", "");
                var response = await client.PostJsonAsync(RoutePrefix, new NewCustomerDto {Email = newEmail, Name = newName});
                response.EnsureSuccessStatusCode();
                var result = await response.Content.DeserializeFromJson<ServiceResult<int>>();
                Assert.NotNull(result);
                var customerId = result.Payload;
                DbHelper.DoDatabaseOperation(context =>
                {
                    var newCustomer = context.Customers.FirstOrDefault(x => x.Id == customerId);
                    Assert.NotNull(newCustomer);
                    Assert.AreEqual(newName, newCustomer.Name);
                    Assert.AreEqual(newEmail, newCustomer.Email);
                });
            }
        }

        private void PrepareData()
        {
            DbHelper.DoDatabaseOperation(context =>
            {
                // Clear everything
                context.Customers.RemoveRange(context.Customers);
                context.Orders.RemoveRange(context.Orders);
                context.SaveChanges();

                // Add fake customers
                context.Customers.Add(new Customer { Name = "John", Email = "john@test.com" });
                context.Customers.Add(new Customer { Name = "Bill", Email = "bill@test.com" });
                context.Customers.Add(new Customer
                {
                    Name = "Fred",
                    Email = "fred@test.com",
                    Orders = new List<Order>
                    {
                        new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 5.99m, Title = "Burder"},
                        new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 3.99m, Title = "Fries"},
                        new Order {CreatedDate = DateTime.UtcNow.AddDays(-1), Price = 2m, Title = "Toy"}
                    }
                });
                context.SaveChanges();
            });
        }
    }
}