﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Albelli.Assignment.Api.Tests.Utils
{
    public static class HttpExtensions
    {
        public static async Task<T> DeserializeFromJson<T>(this HttpContent content)
        {
            using (var streamReader = new StreamReader(await content.ReadAsStreamAsync()))
            using (var reader = new JsonTextReader(streamReader))
                return new JsonSerializer().Deserialize<T>(reader);
        }

        public static async Task<HttpResponseMessage> PostJsonAsync(this HttpClient client, string url, object data)
        {
            var myContent = JsonConvert.SerializeObject(data);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await client.PostAsync(url, byteContent);
        }
    }
}