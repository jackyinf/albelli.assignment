﻿using System;
using Albelli.Assignment.DAL;
using Microsoft.EntityFrameworkCore;

namespace Albelli.Assignment.Api.Tests.Utils
{
    public class DbHelper
    {
        public string ConnectionString { get; }

        public DbHelper(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public void DoDatabaseOperation(Action<ApplicationDbContext> action)
        {
            using (ApplicationDbContext db = new ApplicationDbContext(GetApplicationDbContext()))
            {
                action(db);
            }
        }

        private DbContextOptions<ApplicationDbContext> GetApplicationDbContext()
        {
            DbContextOptions<ApplicationDbContext> options =
                new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlServer(ConnectionString)
                    .Options;
            return options;
        }
    }
}