﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Albelli.Assignment.Domain.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}