﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Albelli.Assignment.Domain.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        [InverseProperty(nameof(Order.Customer))]
        public ICollection<Order> Orders { get; set; }
    }
}