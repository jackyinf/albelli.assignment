﻿namespace Albelli.Assignment.Domain.Dtos.Requests
{
    public class NewOrderDto
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
    }
}