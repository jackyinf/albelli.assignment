﻿namespace Albelli.Assignment.Domain.Dtos.Requests
{
    public class NewCustomerDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}