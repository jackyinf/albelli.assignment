﻿using System.Collections.Generic;
using System.Linq;
using Albelli.Assignment.Domain.Models;

namespace Albelli.Assignment.Domain.Dtos.Results
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public List<OrderDto> Orders { get; set; } = new List<OrderDto>();

        public static class Factory
        {
            public static CustomerDto CreateFrom(Customer customer, bool withOrders = false)
            {
                var dto = new CustomerDto();
                dto.Id = customer.Id;
                dto.Name = customer.Name;
                dto.Email = customer.Email;
                if (withOrders)
                    dto.Orders = customer.Orders?.Select(OrderDto.Factory.CreateFrom).ToList();
                return dto;
            }
        }
    }
}