﻿using System;
using Albelli.Assignment.Domain.Models;

namespace Albelli.Assignment.Domain.Dtos.Results
{
    public class OrderDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }

        public static class Factory
        {
            public static OrderDto CreateFrom(Order order)
            {
                var dto = new OrderDto();
                dto.Id = order.Id;
                dto.Title = order.Title;
                dto.Price = order.Price;
                dto.CreatedDate = order.CreatedDate;
                return dto;
            }
        }
    }
}