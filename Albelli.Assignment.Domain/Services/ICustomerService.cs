﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.Assignment.Domain.Dtos.Requests;
using Albelli.Assignment.Domain.Dtos.Results;

namespace Albelli.Assignment.Domain.Services
{
    public interface ICustomerService
    {
        Task<ServiceResult<List<CustomerDto>>> GetCustomers();

        Task<ServiceResult<List<CustomerDto>>> GetCustomersWithOrders();

        Task<ServiceResult<int>> AddNewOrderForCustomer(int customerId, NewOrderDto orderDto);

        Task<ServiceResult<int>> AddNewCustomer(NewCustomerDto customerDto);
    }
}