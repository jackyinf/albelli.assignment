﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Albelli.Assignment.Domain.Models;

namespace Albelli.Assignment.Domain.Repositories
{
    public interface ICustomerRepository
    {
        Task<int> SaveChangesAsync();

        Task<List<Customer>> GetCustomers();

        Task<Customer> GetCustomer(int customerId);

        Task AddAsync(Customer customer);

        Task<Customer> FindByAsync(string email);
    }
}