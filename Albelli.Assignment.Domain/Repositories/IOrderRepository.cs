﻿using System.Threading.Tasks;
using Albelli.Assignment.Domain.Models;

namespace Albelli.Assignment.Domain.Repositories
{
    public interface IOrderRepository
    {
        Task<int> SaveChangesAsync();

        Task AddAsync(Order order);
    }
}