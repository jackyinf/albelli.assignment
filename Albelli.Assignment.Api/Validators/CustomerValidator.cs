﻿using Albelli.Assignment.Domain.Models;
using FluentValidation;

namespace Albelli.Assignment.Api.Validators
{
    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot be null");
            RuleFor(x => x.Email).NotEmpty().WithMessage("Email cannot be null");
        }
    }
}