﻿using System;
using Albelli.Assignment.Domain.Models;
using FluentValidation;

namespace Albelli.Assignment.Api.Validators
{
    public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title is required");
            RuleFor(x => x.Price).GreaterThan(0).WithMessage("Price cannot be negative or 0");
        }
    }
}