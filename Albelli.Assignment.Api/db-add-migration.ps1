﻿Param(
  [Parameter(Mandatory=$true)][string]$MigrationName
)
dotnet ef migrations add $MigrationName -p ..\Albelli.Assignment.DAL\Albelli.Assignment.DAL.csproj