﻿using System.Threading.Tasks;
using Albelli.Assignment.Domain.Dtos.Requests;
using Albelli.Assignment.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Albelli.Assignment.Api.Controllers
{
    [Route("api/customers")]
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _service;

        public CustomerController(ILogger<CustomerController> logger, ICustomerService service) : base(logger) 
            => _service = service;

        [HttpGet]
        public async Task<IActionResult> GetCustomers()
            => await HandleResultAsync(() => _service.GetCustomers());

        [HttpGet("with-orders")]
        public async Task<IActionResult> GetCustomersWithOrders()
            => await HandleResultAsync(() => _service.GetCustomersWithOrders());

        [HttpPost("{customerId:int}/order")]
        public async Task<IActionResult> AddNewOrderForCustomer([FromRoute] int customerId, [FromBody] NewOrderDto orderDto)
            => await HandleResultAsync(() => _service.AddNewOrderForCustomer(customerId, orderDto));

        [HttpPost]
        public async Task<IActionResult> AddNewCustomer([FromBody] NewCustomerDto customerDto)
            => await HandleResultAsync(() => _service.AddNewCustomer(customerDto));
    }
}