﻿using Albelli.Assignment.Api.Services;
using Albelli.Assignment.DAL;
using Albelli.Assignment.DAL.Repositories;
using Albelli.Assignment.Domain.Repositories;
using Albelli.Assignment.Domain.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Albelli.Assignment.Api
{
    public class Startup
    {
        private const string SettingsKeyDefaultConnection = "DefaultConnection";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                var cs = Configuration.GetConnectionString(SettingsKeyDefaultConnection);
                options.UseSqlServer(cs);
            });

            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "Albelli.Assignment", Version = "v1" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Albelli.Assignment V1");
                c.RoutePrefix = "";
            });
        }
    }
}
