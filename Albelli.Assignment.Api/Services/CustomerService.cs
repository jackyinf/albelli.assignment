﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.Assignment.Api.Validators;
using Albelli.Assignment.Domain.Dtos.Requests;
using Albelli.Assignment.Domain.Dtos.Results;
using Albelli.Assignment.Domain.Factories;
using Albelli.Assignment.Domain.Models;
using Albelli.Assignment.Domain.Repositories;
using Albelli.Assignment.Domain.Services;

namespace Albelli.Assignment.Api.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;

        public CustomerService(ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
        }

        public async Task<ServiceResult<List<CustomerDto>>> GetCustomers()
        {
            var customers = await _customerRepository.GetCustomers();
            var customerDtos = customers
                .Select(customer => CustomerDto.Factory.CreateFrom(customer))
                .ToList();
            return ServiceResultFactory.Success(customerDtos);
        }

        public async Task<ServiceResult<List<CustomerDto>>> GetCustomersWithOrders()
        {
            var customers = await _customerRepository.GetCustomers();
            var customerDtos = customers
                .Select(customer => CustomerDto.Factory.CreateFrom(customer, true))
                .ToList();
            return ServiceResultFactory.Success(customerDtos);
        }

        public async Task<ServiceResult<int>> AddNewOrderForCustomer(int customerId, NewOrderDto orderDto)
        {
            if (customerId <= 0)
                return ServiceResultFactory.Fail<int>("Customer's ID must be positive number");
            if (orderDto == null)
                return ServiceResultFactory.Fail<int>("No order info supplied");

            var customer = await _customerRepository.GetCustomer(customerId);
            if (customer == null)
                return ServiceResultFactory.Fail<int>("Customer with such id was not found");

            var order = new Order();
            order.Title = orderDto.Title;
            order.CreatedDate = DateTime.UtcNow;
            order.Price = orderDto.Price;
            order.CustomerId = customerId;

            var validationResult = await new OrderValidator().ValidateAsync(order);
            if (!validationResult.IsValid)
                return ServiceResultFactory.Fail<int>(String.Join("; ", validationResult.Errors.Select(x => x.ErrorMessage)));

            await _orderRepository.AddAsync(order);
            await _orderRepository.SaveChangesAsync();
            return ServiceResultFactory.Success(order.Id);
        }

        public async Task<ServiceResult<int>> AddNewCustomer(NewCustomerDto customerDto)
        {
            if (customerDto == null)
                return ServiceResultFactory.Fail<int>("No customer info supplied");

            var existingCustomer = await _customerRepository.FindByAsync(customerDto.Email);
            if (existingCustomer != null)
                return ServiceResultFactory.Fail<int>("Email is already taken");

            var customer = new Customer();
            customer.Name = customerDto.Name;
            customer.Email = customerDto.Email;

            var validationResult = await new CustomerValidator().ValidateAsync(customer);
            if (!validationResult.IsValid)
                return ServiceResultFactory.Fail<int>(String.Join("; ", validationResult.Errors.Select(x => x.ErrorMessage)));

            await _customerRepository.AddAsync(customer);
            await _customerRepository.SaveChangesAsync();
            return ServiceResultFactory.Success(customer.Id);
        }
    }
}