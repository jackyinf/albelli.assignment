﻿using System;
using System.Threading.Tasks;
using Albelli.Assignment.Domain.Models;
using Albelli.Assignment.Domain.Repositories;

namespace Albelli.Assignment.DAL.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _context;

        public OrderRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();

        public async Task AddAsync(Order order)
        {
            if (order == null)
                throw new ArgumentException(nameof(order));

            await _context.Orders.AddAsync(order);
        }
    }
}