﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Albelli.Assignment.Domain.Models;
using Albelli.Assignment.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Albelli.Assignment.DAL.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ApplicationDbContext _context;

        public CustomerRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();

        public async Task<List<Customer>> GetCustomers() 
            => await _context.Customers.Include(x => x.Orders).ToListAsync();

        public async Task<Customer> GetCustomer(int customerId) 
            => await _context.Customers.FirstOrDefaultAsync(x => x.Id == customerId);

        public async Task AddAsync(Customer customer)
        {
            if (customer == null)
                throw new ArgumentException(nameof(customer));

            await _context.Customers.AddAsync(customer);
        }

        public async Task<Customer> FindByAsync(string email) 
            => await _context.Customers.FirstOrDefaultAsync(x => x.Email.ToUpperInvariant() == email.ToUpperInvariant());
    }
}