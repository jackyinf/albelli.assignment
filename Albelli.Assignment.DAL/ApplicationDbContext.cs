﻿using Albelli.Assignment.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Albelli.Assignment.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}