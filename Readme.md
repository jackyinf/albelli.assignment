## Assignment for Albelli

### Requirements to run

- Dotnet 2.1 SDK, preferrably latest https://www.microsoft.com/net/download/windows
- MSSQL server (local or docker). I'm using SQL Server 2016

### To run

1. Set correct connection string in appsettings.json before running the app.
2. Run Albelli.Assignment.Api application. It should automatically create database and do a migration.

### To test

Unit tests are located in Albelli.Assignment.Api.Tests project in folder UnitTests.
(They can be run using Resharper)

There are also integration tests, which do API calls against Api's routes and test, if items go into database.
For integration tests, check your connection string and change it to point to test database.
Database does not need to exist beforehand. When running tests, it will automatically create it and do all the migrations.